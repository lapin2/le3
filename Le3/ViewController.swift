//
//  ViewController.swift
//  Le3
//
//  Created by student on 25.08.18.
//  Copyright © 2018 lapin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

       perfectNumber(28)

    }

    func biggestNumber(firstNumber: Int, secondNumber:Int) {
        var result = 0
        if firstNumber > secondNumber {
            result = firstNumber
        }
        if secondNumber > firstNumber {
            result = secondNumber
        }
        print ("the biggest number is \(result)")
    }

    func squareOfNumber(_ number: Double) {
        let square = number * number
        print ("square of number \(number) is \(square)")
    }

    func cubeOfNumber(_ number: Double) {
        let cube = number * number * number
        print ("cube of number \(number) is \(cube)")
    }

    func printNumbersBeforeAndAfter(_ number: Int) {
        var printedNumber = -1
        while printedNumber < number {
            printedNumber = printedNumber + 1
            print (printedNumber)
        }
        while printedNumber > 0 {
            printedNumber = printedNumber - 1
            print (printedNumber)
        }
    }

    func dividers(_ number: Int) {
        var measure = 0
        var quantityOfDividers = 0
        while measure < number {
            measure = measure + 1
            if number % measure == 0 {
                print ("number \(number) could be divided by \(measure)")
                quantityOfDividers = quantityOfDividers + 1
            }
        }
        print ("number \(number) has \(quantityOfDividers) dividers")
    }

    func perfectNumber(_ number: Int) {
        var measure = 1
        var sumOfDividers = 0
        while measure < number {
            if number % measure == 0 {
                sumOfDividers = sumOfDividers + measure
            }
            measure = measure + 1
        }
        if sumOfDividers == number {
            print ("\(number) is a perfect number")
        }
        else {
            print ("\(number) is not a perfect number")
        }
    }







}

